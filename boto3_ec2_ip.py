import boto3

AWS_REGION = "us-east-2"
KEY_PAIR_NAME = 'suri-ohio'
AMI_ID = 'ami-0beaa649c482330f7' # Amazon Linux 2
SECURITY_GROUP_ID = 'sg-08bda931d6a177bb4'

USER_DATA = '''#!/bin/bash
yum update
'''

EC2_RESOURCE = boto3.resource('ec2', region_name=AWS_REGION)
EC2_CLIENT = boto3.client('ec2', region_name=AWS_REGION)

instances = EC2_RESOURCE.create_instances(
    MinCount = 1,
    MaxCount = 1,
    ImageId=AMI_ID,
    InstanceType='t2.micro',
    KeyName=KEY_PAIR_NAME,
    SecurityGroupIds = [SECURITY_GROUP_ID],
    UserData=USER_DATA,
    TagSpecifications=[
        {
            'ResourceType': 'instance',
            'Tags': [
                {
                    'Key': 'Name',
                    'Value': 'suri-ec2-instance'
                },
            ]
        },
    ]
)



for instance in instances:
    print(f'EC2 instance "{instance.id}" has been launched')
    
    instance.wait_until_running()
    

    print(f'EC2 instance "{instance.id}" has been started')
    print(f'EC2 instance "{instance.private_ip_address}" ip-private')
